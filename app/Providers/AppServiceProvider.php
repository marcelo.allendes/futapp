<?php

namespace App\Providers;
use App\Src\Users\Domain\Contracts\UserInterface;
use App\Src\Users\Infrastructure\Database\UserRepository;
/*includeInterface*/

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(UserInterface::class, UserRepository::class);
        /*AquiNuevoDomain*/
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
