<?php

namespace App\Console\Commands\Traits\CreateEntityDomain;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

trait  ProcessDTOProperties
{
    public function processDTOProperties(string $content): string
    {
        $propertyDefinitions = $this->getDTODefinitionField();
        $content = str_replace('/*PropertiesDtos*/', $propertyDefinitions, $content);

        return $content;
    }


    public function getDTODefinitionField(): string
    {
        $result = "";
        foreach ($this->getColumns() as $column) {
            $result.= sprintf('public $%s,',  Str::camel($column))."
        ";
        }
        return $result;
    }
}
