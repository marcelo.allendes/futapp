<?php

namespace App\Console\Commands\Traits\CreateEntityDomain;

use Illuminate\Support\Str;

trait  RouteProcessor
{

    public function goProcessRoute(string $content): string
    {
        $module = str_replace('_','-',$this->table);
        $singleName = substr($this->entityName,0,-1);
        $variableName = Str::camel(substr($this->entityName,0,-1));
        $routes = sprintf("Route::prefix('%s')->group(function () {
    Route::get('/', [UserController::class, 'index']);
    Route::post('create', [UserController::class, 'create']);
    Route::prefix('{id}')->group(function () {
        Route::get('/', [UserController::class, 'show']);
        Route::put('update', [UserController::class, 'update']);
        Route::delete('/', [UserController::class, 'delete']);
    });
});", $module);
        $routes = str_replace("UserController", $singleName."Controller",$routes);
        $content = str_replace("/**NewModule**/", $routes, $content);
        $content.="
/**NewModule**/";

        $useControllers = sprintf("use App\Src\%s\Infrastructure\Controllers\%sController;",
            $this->entityName,
            $singleName
        );
        $useControllers.="
/*includeControllers*/";
        $content = str_replace("/*includeControllers*/", $useControllers, $content);
        return $content;
    }
}
