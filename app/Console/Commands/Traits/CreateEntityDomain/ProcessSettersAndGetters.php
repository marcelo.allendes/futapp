<?php

namespace App\Console\Commands\Traits\CreateEntityDomain;

use Illuminate\Support\Str;

trait  ProcessSettersAndGetters
{
    public function doProcessSettersAndGetters(string $content): string
    {
        $propertyDefinitions = $this->getDefinitionFieldGettersAndSetters();
        $content = str_replace('//**GettersAndSetters', $propertyDefinitions, $content);

        return $content;
    }


    public function getDefinitionFieldGettersAndSetters(): string
    {
        $result = "";
        foreach ($this->getColumns() as $column) {
            $result .= sprintf('%s
    %s', $this->processGetter($column), $this->processSetter($column));
        }

        return $result;
    }

    public function processGetter(string $column): string
    {
        $column = Str::camel($column);
        $nameFunction = ucfirst($column);

        $result = sprintf('
    public function get%s()
    {
        return $this->%s;
    }', $nameFunction, $column);

        return $result;
    }

    public function processSetter(string $column): string
    {
        $column = Str::camel($column);
        $nameFunction = ucfirst($column);

        $result = sprintf('public function set%s($%s): static
    {
        $this->%s = $%s;
        return $this;
    }', $nameFunction, $column, $column, $column);

        return $result;
    }
}
