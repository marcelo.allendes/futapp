<?php

namespace App\Console\Commands\Traits\CreateEntityDomain;

use Illuminate\Support\Str;

trait  ProcessResourceProperties
{

    public function getResourceField(string $content): string
    {

        $singleName = substr($this->entityName,0,-1);
        $result = "";
        foreach ($this->getColumns() as $column) {
            $columnCamel = Str::camel($column);
            $result.= sprintf('"%s"=>$this->get%s()', $column, lcfirst($columnCamel)).",
            ";
        }
        $content = str_replace('/*FieldsResources*/', $result, $content);

        return $content;
    }
}
