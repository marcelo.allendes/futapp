<?php

namespace App\Src\Users\Infrastructure\Resources;

use App\Src\Users\Domain\Entities\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserShowResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var User $this */
        return [
            /*FieldsResources*/
        ];
    }
}
