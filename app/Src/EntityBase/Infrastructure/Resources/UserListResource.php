<?php

namespace App\Src\Users\Infrastructure\Resources;

use App\Src\Users\Infrastructure\Database\UserMapper;
use Illuminate\Database\Eloquent\Collection;

class UserListResource
{
    public static function  list(Collection $data): array
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = new UserShowResource(UserMapper::toDomainEntity($item));
        }
        return $result;
    }
}
