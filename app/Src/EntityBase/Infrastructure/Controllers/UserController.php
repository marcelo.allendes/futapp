<?php

namespace App\Src\Users\Infrastructure\Controllers;

use App\Src\Users\Application\UsesCases\UserAllSearcher;
use App\Src\Users\Application\UsesCases\UserCreator;
use App\Src\Users\Application\UsesCases\UserDeleter;
use App\Src\Users\Application\UsesCases\UserFinder;
use App\Src\Users\Application\UsesCases\UserUpdater;
use App\Src\Users\Infrastructure\Database\UserMapper;
use App\Src\Users\Infrastructure\Resources\UserListResource;
use App\Src\Users\Infrastructure\Resources\UserShowResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class UserController
{
    public function index(UserAllSearcher $allSearcher):JsonResponse
    {
        $users = $allSearcher->__invoke();
        return response()->json(UserListResource::list($users));

    }

    public function show(UserFinder $userFinder, int $userId):JsonResponse
    {
        $user = $userFinder->__invoke($userId);
        return response()->json(new UserShowResource($user));

    }

    public function create(Request $request, UserCreator $userCreator):JsonResponse
    {
        $user = $userCreator->__invoke($request->all());
        return response()->json(new UserShowResource($user));

    }

    public function update(Request $request, UserUpdater $userUpdater, int $id):JsonResponse
    {
        $user = UserMapper::toDomainEntity($userUpdater->__invoke($request->all(), $id));
        return response()->json(new UserShowResource($user));

    }

    public function delete(Request $request, UserDeleter $userDeleter, int $id):JsonResponse
    {
        $userDeleter->__invoke( $id);
        return response()->json([]);

    }
}
