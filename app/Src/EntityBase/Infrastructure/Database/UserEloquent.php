<?php

namespace App\Src\Users\Infrastructure\Database;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserEloquent extends Model
{
    use HasFactory, Notifiable;

    public $timestamps = true;

    /*nameTableEloquent*/

}
