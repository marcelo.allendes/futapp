<?php

namespace App\Src\Users\Infrastructure\Database;

use App\Src\Users\Domain\DTO\UserDTO;
use App\Src\Users\Domain\Entities\User;
use Illuminate\Support\Str;
use ReflectionClass;

class UserMapper
{

    public static function toDomainEntity(UserEloquent $userEloquent): User
    {
        $attributes = self::toCamelCase($userEloquent->attributesToArray());
        $user = new User($attributes);
        foreach ($userEloquent->getRelations() as $relationName =>$relation){
            if(property_exists($user, $relationName)) {
                $functionName = "set{$relationName}";
                $user->$functionName($relation) ;
            }
        }
        return $user;
    }

    protected static function toCamelCase(array $attributes): array
    {
        $camels = [];
        foreach ($attributes as $key => $value) {
            $camels[Str::camel($key)] = $value;
        }
        return $camels;
    }

    public static function toEloquentModel(User $user): UserEloquent
    {
        $eloquentUser = new UserEloquent();
        $reflectionClass = new ReflectionClass($user);
        $reflectionProperties = $reflectionClass->getProperties();
        foreach ($reflectionProperties as $reflectionProperty) {
            $nameAttributeSnake = Str::snake($reflectionProperty->getName());
            $methodGet = "get".ucfirst($reflectionProperty->getName());//Con esto obtengo las propiedades de una clase de dominio de una forma dinamica
            $eloquentUser->$nameAttributeSnake = $user->$methodGet();
        }
        return $eloquentUser;
    }

    public static function toEloquentModelUpdate(UserEloquent $userEloquent, UserDTO $userDTO): UserEloquent
    {
        $attributes = get_object_vars($userDTO);
        foreach ($attributes as $attribute=>$value) {
            $camelCaseAttribute = Str::snake($attribute);//en la clase de dominio se usa camel case y en la bd snake
            if(key_exists($camelCaseAttribute, $userEloquent->attributesToArray())){
                $userEloquent->$camelCaseAttribute = $value;
            }
        }
        return $userEloquent;
    }

}
