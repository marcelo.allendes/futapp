<?php

namespace App\Src\Users\Application\Actions;

use App\Src\Users\Domain\Contracts\UserInterface;
use App\Src\Users\Infrastructure\Database\UserEloquent;

class UserDestroy
{
    public function __construct(private readonly UserInterface $userInterface)
    {
    }

    public function __invoke(UserEloquent $user): void
    {
        $this->userInterface->destroy($user);
    }
}
