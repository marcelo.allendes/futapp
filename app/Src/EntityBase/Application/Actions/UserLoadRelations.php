<?php

namespace App\Src\Users\Application\Actions;

use App\Src\Users\Domain\Contracts\UserInterface;
use App\Src\Users\Infrastructure\Database\UserEloquent;

class UserLoadRelations
{
    public function __construct(private readonly UserInterface $userInterface)
    {
    }

    public function __invoke(UserEloquent $userEloquent, array $relations): UserEloquent
    {
        return $this->userInterface->loadRelations($userEloquent, $relations);
    }
}
