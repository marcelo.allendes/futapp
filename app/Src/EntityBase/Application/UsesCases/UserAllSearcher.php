<?php

namespace App\Src\Users\Application\UsesCases;

use App\Src\Users\Domain\Contracts\UserInterface;
use Illuminate\Database\Eloquent\Collection;

class UserAllSearcher
{
    public function __construct(private readonly UserInterface $userInterface)
    {
    }

    public function __invoke():Collection
    {
        return $this->userInterface->all();

    }
}
