<?php

namespace App\Src\Users\Application\UsesCases;
use App\Src\Users\Infrastructure\Database\UserEloquent;
use App\Src\Users\Application\Actions\UserDestroy;
use App\Src\Users\Infrastructure\Database\UserMapper;

class UserDeleter
{
    public function __construct(
        private readonly UserDestroy $userDestroy,
        private readonly UserFinder  $userFinder

    ){

    }
    public function __invoke(int $id): void
    {
        $user =  UserMapper::toEloquentModel($this->userFinder->__invoke($id));
        $this->userDestroy->__invoke($user);
    }
}
