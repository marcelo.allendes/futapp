<?php

namespace App\Src\Users\Domain\Entities;

use Illuminate\Support\Str;

class User
{
    //**PropertiesZone



    public function __construct(array $attributes = [])
    {
        foreach ($attributes as $key => $value) {
            $property = Str::camel($key);
            if(property_exists($this, $property)) {
                $this->{$key} = $value;
            }
        }
    }

    public function setFromArray(array $attributes): void
    {
        foreach ($attributes as $key => $value) {
            if(property_exists($this, $key)) {
                $fieldToSet = "set{$key}";
                $this->$fieldToSet($value);
            }
        }

    }


    //**GettersAndSetters


}
