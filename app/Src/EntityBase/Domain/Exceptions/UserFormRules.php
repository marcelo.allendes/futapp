<?php

namespace App\Src\Users\Domain\Exceptions;
use App\Src\Users\Domain\DTO\UserDTO;
use Illuminate\Support\Facades\Validator;

class UserFormRules
{
    public static function validate(UserDTO $userDTO)
    {
        $data = $userDTO->toArray();
        return Validator::make($data, [

        ]);
    }
}
