<?php

namespace App\Src\Users\Application\UsesCases;

use App\Src\Users\Application\Actions\UserFind;
use App\Src\Users\Application\Actions\UserLoadRelations;
use App\Src\Users\Domain\Entities\User;
use App\Src\Users\Infrastructure\Database\UserMapper;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserFinder
{
    public function __construct(
        private readonly UserFind $userFind,
        private readonly UserLoadRelations $userLoadRelations
    )
    {
    }

    public function __invoke($id): User
    {
        $userEloquent = $this->userFind->__invoke($id);
        if(!$userEloquent){
            throw new HttpResponseException(response()->json([
                'message' => sprintf('User <%s> no encontrado', $id),
                'code' => 404,
            ], 422));
        }
        $userEloquent = $this->userLoadRelations->__invoke($userEloquent, ['center']);
        $user = UserMapper::toDomainEntity($userEloquent);

        return $user;
    }
}
