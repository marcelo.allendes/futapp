<?php

namespace App\Src\Users\Application\UsesCases;

use App\Src\Shared\Domain\FormRulesCustomExceptions;
use App\Src\Users\Application\Actions\UserPersist;
use App\Src\Users\Domain\DTO\UserDTO;
use App\Src\Users\Domain\Exceptions\UserFormRules;
use App\Src\Users\Infrastructure\Database\UserEloquent;
use App\Src\Users\Infrastructure\Database\UserMapper;

class UserUpdater
{
    public function __construct(
        private readonly UserPersist $userPersist,
        private readonly UserFinderEloquent  $userFinderEloquent

    ){

    }
    public function __invoke(array $data, int $id): UserEloquent
    {
        $user = $this->userFinderEloquent->__invoke($id);
        $userDto = new UserDTO($id, $data['name'], $data['email'], $data['password'], $data['remember_token']);

        $validator = UserFormRules::validate($userDto);
        if ($validator->fails()) {
            throw new FormRulesCustomExceptions($validator);
        }
        $user = UserMapper::toEloquentModelUpdate($user, $userDto);
        return $this->userPersist->__invoke($user);
    }
}
