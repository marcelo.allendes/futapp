<?php

namespace App\Src\Users\Application\UsesCases;

use App\Src\Users\Application\Actions\UserDestroy;
use App\Src\Users\Application\UserFinder;

class UserDeleter
{
    public function __construct(
        private readonly UserDestroy $userDestroy,
        private readonly UserFinder  $userFinder

    ){

    }
    public function __invoke(int $id): void
    {
        $this->userDestroy->__invoke($this->userFinder->__invoke($id));
    }
}
