<?php

namespace App\Src\Users\Application\UsesCases;

use App\Src\Users\Application\Actions\UserStore;
use App\Src\Users\Domain\DTO\UserDTO;
use App\Src\Users\Domain\Entities\User;
use App\Src\Users\Domain\Exceptions\UserFormRules;
use App\Src\Shared\Domain\FormRulesCustomExceptions;

class UserCreator
{
    public function __construct(private readonly UserStore $userStore){

    }
    public function __invoke(array $data): User
    {
        $userDto = new UserDTO(null, $data['name'], $data['email'], $data['password'], $data['remember_token']);
        $validator = UserFormRules::validate($userDto);
        if ($validator->fails()) {
            throw new FormRulesCustomExceptions($validator);
        }
        $user = new User($userDto->toArray());
        return $this->userStore->__invoke($user);
    }
}
