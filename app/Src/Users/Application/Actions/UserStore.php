<?php

namespace App\Src\Users\Application\Actions;

use App\Src\Users\Domain\Contracts\UserInterface;
use App\Src\Users\Domain\Entities\User;

class UserStore
{
    public function __construct(private readonly UserInterface $userInterface)
    {
    }

    public function __invoke(User $user): User
    {
        return $this->userInterface->store($user);
    }
}
