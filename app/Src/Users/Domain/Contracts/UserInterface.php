<?php

namespace App\Src\Users\Domain\Contracts;

use App\Src\Users\Domain\Entities\User;
use App\Src\Users\Infrastructure\Database\UserEloquent;
use Illuminate\Database\Eloquent\Collection;

interface UserInterface
{
    public function all(): Collection;

    public function find(int $id): ?UserEloquent;

    public function store(User $user): User;

    public function destroy(UserEloquent $userEloquent): void;

    public function persist(UserEloquent $userEloquent): UserEloquent;

    public function loadRelations(UserEloquent $userEloquent, array $relations): UserEloquent;

}
