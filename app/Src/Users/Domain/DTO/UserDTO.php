<?php

namespace App\Src\Users\Domain\DTO;

class UserDTO
{


    public function __construct(
        public ?int $id = null,
        public string $name = "",
        public string $email = "",
        public string $password = "",
        public string $rememberToken = "",
    ){

    }

    public function toArray(): array
    {
        $attributes = get_object_vars($this);

        return $attributes;
    }
}
