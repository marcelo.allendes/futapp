<?php

namespace App\Src\Users\Domain\Entities;

use Illuminate\Support\Str;

class User
{
    private ?int $id;
    private string $name;
    private string $email;
    private string $password;
    private ?string $rememberToken = null;

    private $center;

    //Las entidadesde relacionadas se declaran en el contructor
    //private collection $userContacts;
    //private Sucursal $sursal;
    public function __construct(array $attributes = [])
    {
        foreach ($attributes as $key => $value) {
            $property = Str::camel($key);
            if(property_exists($this, $property)) {
                $this->{$key} = $value;
            }
        }
    }

    public function setFromArray(array $attributes): void
    {
        foreach ($attributes as $key => $value) {
            if(property_exists($this, $key)) {
                $fieldToSet = "set{$key}";
                $this->$fieldToSet($value);
            }
        }

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;
        return $this;
    }

    public function setId(?int $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;
        return $this;
    }

    public function getRememberToken(): ?string
    {
        return $this->rememberToken;
    }

    public function setRememberToken(string $rememberToken): static
    {
        $this->rememberToken = $rememberToken;
        return $this;
    }

    public function getCenter(){
        return $this->center;
    }

    public function setCenter($center): static
    {
        $this->center = $center;
        return $this;
    }



}
