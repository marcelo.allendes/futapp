<?php
namespace App\Src\Shared\Domain;


use Exception;
use Illuminate\Support\Facades\Validator;

class FormRulesCustomExceptions extends Exception
{
    public function __construct($validator, $code = 0, Exception $previous = null)
    {
        $message = $validator->errors();
        parent::__construct($message, $code, $previous);
    }

    public function report()
    {
        // Aquí puedes definir la lógica para reportar la excepción, como enviar un correo, loguear a un servicio externo, etc.
    }

    public function render($request)
    {
        // Aquí defines la respuesta que se debe retornar cuando la excepción se lanza.
        return response()->json([
            'error' => 'Validation Error',
            'message' => $this->getMessage(),
            'code' => DomainCodeException::VALIDATION_FORM_RULES_CODE
        ], 422);
    }
}
