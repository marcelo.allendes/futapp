<?php
use App\Src\Users\Infrastructure\Controllers\UserController;
/*includeControllers*/
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    dd("Api respondiendo");
});

Route::prefix('users')->group(function () {
    Route::get('/', [UserController::class, 'index']);
    Route::post('create', [UserController::class, 'create']);
    Route::prefix('{userId}')->group(function () {
        Route::get('/', [UserController::class, 'show']);
        Route::put('update', [UserController::class, 'update']);
        Route::delete('/', [UserController::class, 'delete']);
    });
});

/**NewModule**/
